﻿using Aliaga_Final.Models;
using Aliaga_Final.Models.Entidades;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Aliaga_Final.Controllers
{
    public class UsuarioController : Controller
    {

        public FinalContext cnx;
        public readonly IConfiguration configuration;
        public UsuarioController(FinalContext cnx, IConfiguration configuration)
        {
            this.configuration = configuration;
            this.cnx = cnx;
        }


        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string passwd)
        {
            var usuarioLogeado = cnx.Usuarios.Where(o => o.username == username && o.passwd == passwd)
                .FirstOrDefault();

            if (usuarioLogeado != null)
            {
                // Autenticaremos
                var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, username)
                };

                var claimsIdentity = new ClaimsIdentity(claims, "Login");
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                HttpContext.SignInAsync(claimsPrincipal);

                return RedirectToAction("Index", "Usuario");
                
            }
            return View();

        }

        [Authorize]
        public ActionResult Index()
        {

            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = cnx.Usuarios.Where(o => o.username == claim.Value).First();
            ViewBag.Claim = claim;
            ViewBag.User = user;


            var notas = cnx.Notas.Include("notaEtiquetas.etiqueta")
                .Include(o=>o.usuario).Where(o=>o.idUsuario == user.idUsuario).ToList();
            var etiquetas = cnx.Etiquetas.ToList();
            ViewBag.Notas = notas;
            ViewBag.Etiquetas = etiquetas;
            return View();
        }



        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string username, string passwd, string repasswd)
        {
            var usuarioLogeado = cnx.Usuarios.Where(o => o.username == username && o.passwd == passwd)
                .FirstOrDefault();

            if (usuarioLogeado != null)
            {
                return View();
            }
            else
            {
                if (passwd == repasswd)
                {
                    var u = new Usuario();
                    u.username = username;
                    u.passwd = passwd;

                    cnx.Usuarios.Add(u);
                    cnx.SaveChanges();
                    return RedirectToAction("Login", "Usuario");
                }
                else
                {
                    return View();
                }
               
            }
        }


        [HttpGet]
        public ActionResult CreateNota()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateNota(string titulo, string contenido)
        {

            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = cnx.Usuarios.Where(o => o.username == claim.Value).FirstOrDefault();
            ViewBag.Claim = claim;
            ViewBag.User = user;

            var nota = new Nota();
            //nota.idNota = cnx.Notas.AsEnumerable().OrderByDescending(o => o.idNota).FirstOrDefault().idNota + 1;
            nota.titulo = titulo;
            nota.contenido = contenido;
            nota.fechaMod = DateTime.Now;
            nota.idUsuario = user.idUsuario;

            cnx.Notas.Add(nota);
            cnx.SaveChanges();
            return RedirectToAction("Index", "Usuario");
            
        }

        [Authorize]
        public ActionResult VerNotas(int idEtiqueta)
        {
            var notaetiquetas = cnx.NotaEtiquetas.Include(o=>o.etiqueta).Include("nota.notaEtiquetas").Where(o=>o.etiqueta.idEtiqueta == idEtiqueta).ToList();
            var etiquetas = cnx.Etiquetas.ToList();
            ViewBag.Notas = notaetiquetas;
            ViewBag.Etiquetas = etiquetas;
            return View();
        }
        
             [Authorize]
        public ActionResult DetallesNota(int idNota)
        {
            var nota = cnx.Notas.Include("notaEtiquetas.etiqueta").Where(o => o.idNota == idNota).FirstOrDefault();
            
            ViewBag.Nota = nota;
            
            return View();
        }
        
        [Authorize]
        [HttpGet]
        public ActionResult EditarNota(int idNota)
        {
            var nota = cnx.Notas.Include("notaEtiquetas.etiqueta").Where(o => o.idNota == idNota).FirstOrDefault();
            var etiquetas = cnx.Etiquetas.ToList();
            ViewBag.Nota = nota;
            ViewBag.IdNota = idNota;
            ViewBag.Etiquetas = etiquetas;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditarNota(int idNota, string titulo, string contenido)
        {
            var nota = cnx.Notas.Include("etiquetas.etiqueta").Include("etiquetas.nota").Where(o => o.idNota == idNota).FirstOrDefault();
            nota.titulo = titulo;
            nota.contenido = contenido;
            nota.fechaMod = DateTime.Now;
            cnx.Update(nota);
            cnx.SaveChanges();

            return RedirectToAction("Index","Usuario");
        }

        [HttpPost]
        public ActionResult QuitarEtiqueta(int idNotaEtiqueta)
        {
            var nota = cnx.NotaEtiquetas.Where(o=>o.idNotaEtiqueta == idNotaEtiqueta).FirstOrDefault();
            cnx.NotaEtiquetas.Remove(nota);
           
            cnx.SaveChanges();

            return RedirectToAction("Index","Usuario");
        }
        [HttpPost]
        public ActionResult AgregarEtiqueta(int idNota, int idEtiqueta)
        {
            var nota = new NotaEtiqueta();
            nota.idEtiqueta = idEtiqueta;
            nota.idNota = idNota;
            cnx.NotaEtiquetas.Add(nota);
            cnx.SaveChanges();

            return RedirectToAction("Index", "Usuario");
        }

        [HttpPost]
        public ActionResult EliminarNota(int idNota)
        {
            var nota = cnx.Notas.Where(o => o.idNota == idNota).FirstOrDefault();
            cnx.Notas.Remove(nota);

            cnx.SaveChanges();

            return RedirectToAction("Index", "Usuario");
        }
        
         [Authorize]
        [HttpGet]
        public ActionResult CompartirNota()
        {
            
            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = cnx.Usuarios.Where(o => o.username == claim.Value)
                .FirstOrDefault();
            ViewBag.UsuarioLogeado = user;
            var notas = cnx.Notas.Where(o=>o.idUsuario == user.idUsuario)
                .Include("notaEtiquetas.nota").ToList();

            ViewBag.Notas = notas;

            var usuarios = cnx.Usuarios.ToList();

            ViewBag.Usuarios = usuarios;

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult CompartirNota(int idNota, int idUsuario)
        {
            if (cnx.NotaCompartidas.Where(o=>o.idNota == idNota && o.idUsuario==idUsuario).FirstOrDefault() == null)
            {
                var notaCompartida = new NotaCompartida();

                notaCompartida.idNota = idNota;
                notaCompartida.idUsuario = idUsuario;
                cnx.NotaCompartidas.Add(notaCompartida);
                cnx.SaveChanges();

                return RedirectToAction("NotasCompartidas", "Usuario");
            }
            else
            {
                return RedirectToAction("Index","Usuario");
            }

            
        }


        [Authorize]
        [HttpGet]
        public ActionResult NotasCompartidas()
        {

            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = cnx.Usuarios.Where(o => o.username == claim.Value)
                .FirstOrDefault();
            ViewBag.UsuarioLogeado = user;
            var notascompartidas = cnx.NotaCompartidas
                .Where(o => o.idUsuario == user.idUsuario)
                .Include(o=>o.nota).ToList();

            ViewBag.NotaCompartidas = notascompartidas;

           

            return View();
        }


    }
}
