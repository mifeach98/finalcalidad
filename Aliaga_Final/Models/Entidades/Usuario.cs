﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Entidades
{
    public class Usuario
    {
        public int idUsuario { get; set; }
        public string username { get; set; }
        public string passwd { get; set; }


        public List<Nota> notas { get; set; }
        public List<NotaCompartida> notaCompartidas { get; set; }
    }
}
