﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Entidades
{
    public class NotaCompartida
    {
        public int idNotaCompartida { get; set; }
        public int idNota { get; set; }
        public int idUsuario { get; set; }


        public Nota nota { get; set; }
        public Usuario usuario { get; set; }
    }
}
