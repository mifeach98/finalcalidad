﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Entidades
{
    public class Etiqueta
    {
        public int idEtiqueta { get; set; }
        public string nombre { get; set; }

        
        public List<Nota> notas { get; set; }
    }
}
