﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Entidades
{
    public class NotaEtiqueta
    {
        public int idNotaEtiqueta { get; set; }
        public int idNota { get; set; }
        public int idEtiqueta { get; set; }

        public Nota nota { get; set; }
        public Etiqueta etiqueta { get; set; }
    }
}
