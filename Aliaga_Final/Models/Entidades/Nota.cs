﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Entidades
{
    public class Nota
    {
        public int idNota { get; set; }
        public string titulo { get; set; }
        public string contenido{ get; set; }
        public DateTime fechaMod { get; set; }
        public int idUsuario { get; set; }

        public List<NotaEtiqueta> notaEtiquetas { get; set; }
        public Usuario usuario { get; set; }
        
    }
}
