﻿using Aliaga_Final.Models.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Maps
{
    public class NotaEtiquetaMap : IEntityTypeConfiguration<NotaEtiqueta>
    {
        public void Configure(EntityTypeBuilder<NotaEtiqueta> builder)
        {
            builder.ToTable("NotaEtiqueta");
            builder.HasKey(o=>o.idNotaEtiqueta);

            builder.HasOne(o => o.nota).WithMany(o => o.notaEtiquetas).HasForeignKey(o=>o.idNota);
            builder.HasOne(o => o.etiqueta).WithMany().HasForeignKey(o=>o.idEtiqueta);

        }

    }
}
