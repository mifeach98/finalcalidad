﻿using Aliaga_Final.Models.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Maps
{
    public class NotaMap : IEntityTypeConfiguration<Nota>
    {
        public void Configure(EntityTypeBuilder<Nota> builder)
        {
            builder.ToTable("Nota");
            builder.HasKey(o=>o.idNota);

            builder.HasOne(o => o.usuario).WithMany(o => o.notas).HasForeignKey(o=>o.idUsuario);
            builder.HasMany(o => o.notaEtiquetas).WithOne(o=>o.nota).HasForeignKey(o=>o.idNotaEtiqueta);

        }
    }
}
