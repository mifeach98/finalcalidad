﻿using Aliaga_Final.Models.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Maps
{
    public class EtiquetaMap : IEntityTypeConfiguration<Etiqueta>
    {
        public void Configure(EntityTypeBuilder<Etiqueta> builder)
        {
            builder.ToTable("Etiqueta");
            builder.HasKey(o=>o.idEtiqueta);

            builder.HasMany(o => o.notas).WithOne()
                .HasForeignKey(o => o.idNota);
        }
    }
}
