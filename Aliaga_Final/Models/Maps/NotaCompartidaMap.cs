﻿using Aliaga_Final.Models.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aliaga_Final.Models.Maps
{
    public class NotaCompartidaMap : IEntityTypeConfiguration<NotaCompartida>
    {
        public void Configure(EntityTypeBuilder<NotaCompartida> builder)
        {
            builder.ToTable("NotaCompartida");
            builder.HasKey(o=>o.idNotaCompartida);

            builder.HasOne(o => o.nota).WithMany().HasForeignKey(o=>o.idNota);
            builder.HasOne(o => o.usuario).WithMany(o => o.notaCompartidas).HasForeignKey(o=>o.idUsuario);
        }
    }
}
